package com.example.eurekaclient.dao;

import com.example.eurekaclient.model.RepoInfo;

public interface RepoMapper {
    int update(RepoInfo repoInfo);
    int getAmount(String productId);

}
